# Collected CDDB files

This is a copy of the source files and assets of the 2006-era FreeDB,
taken from [here](https://mirror.jcx.life/freedb).

Since this stuff has a habit of disappearing from the Internet, I
put this up as hedge against that happening again.
